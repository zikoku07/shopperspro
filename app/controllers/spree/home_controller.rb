module Spree
  class HomeController < Spree::StoreController
    helper 'spree/products'
    respond_to :html

    def index
      @searcher = build_searcher(params.merge(include_images: true))
      @products = @searcher.retrieve_products
      @products = @products.includes(:possible_promotions) if @products.respond_to?(:includes)
      @taxonomies = Spree::Taxonomy.includes(root: :children)
    end

    def change_product_view

    end

    def compare_product

    end

    def contact

    end

    def blog

    end

    def blog_show
      
    end

    def manufacturers

    end

    def manufacturer_details

    end
  end
end
